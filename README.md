# Users manager

## Description
Users manages is simple app, that lets you manage system user's accounts en masse.

## Usage examples

First I'll create new users _database_. You can see the table below.

| username | password   |
| -------- | ---------- |
| user01   | password01 |
| user02   | password02 |
| user03   | password03 |

You must remember to name columns the same as in the above example (e.g. name, password) - names are case-sensitive - use only lowercase.

_Users manager_ uses CSV format to create users according to database. Example:

``` 
username, password
user01, password01
user02, password02
user03, password03
```

To create users described in CSV file, use command:

```bash
# ./users_manager --add --csv users_list.csv
```

To remove users described in CSV file, use command:

```bash
# ./users_manager --remove --csv users_list.csv
```

